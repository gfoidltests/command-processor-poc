﻿using System;
using System.ServiceModel;
using Command_Processor_PoC.Contracts;

namespace Command_Processor_PoC.Client
{
	static class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Waiting for server. Hit any key to continue...");
			Console.ReadKey();

			ChannelFactory<ICommandProcessor> channelFactory = new ChannelFactory<ICommandProcessor>(
				new NetNamedPipeBinding(),
				new EndpointAddress("net.pipe://localhost/commandProcessor"));

			try
			{
				channelFactory.Open();
				Console.WriteLine("Channel open");

				ICommandProcessor proxy = channelFactory.CreateChannel();

				Command command 		= new HelloCommand();
				proxy.Process(command);

				command = new GriaßdiCommand { Name = "Himen" };
				proxy.Process(command);
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.Error.WriteLine(ex);
				Console.ResetColor();
				channelFactory.Abort();
			}

			Console.WriteLine("\nbye");
		}
	}
}