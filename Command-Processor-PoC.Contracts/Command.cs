﻿namespace Command_Processor_PoC.Contracts
{
	public abstract class Command
	{
		public abstract void Do();
	}
}