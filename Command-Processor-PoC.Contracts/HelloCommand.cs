﻿using System;

namespace Command_Processor_PoC.Contracts
{
	public class HelloCommand : Command
	{
		public override void Do()
		{
			Console.WriteLine($"Hello at {DateTime.Now}");
		}
	}
	//-------------------------------------------------------------------------
	public class GriaßdiCommand : Command
	{
		public string Name { get; set; }
		//---------------------------------------------------------------------
		public override void Do()
		{
			Console.WriteLine($"Griaß di {this.Name} um {DateTime.Now}");
		}
	}
}