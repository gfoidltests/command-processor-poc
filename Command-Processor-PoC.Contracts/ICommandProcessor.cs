﻿using System.ServiceModel;

namespace Command_Processor_PoC.Contracts
{
	[ServiceContract]
	public interface ICommandProcessor
	{
		[OperationContract(IsOneWay = true)]
		[ServiceKnownType("GetKnownTypes", typeof(KnownTypes))]     // methods must be static
		void Process(Command command);
	}
}