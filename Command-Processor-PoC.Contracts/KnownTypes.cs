﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace Command_Processor_PoC.Contracts
{
	public static class KnownTypes
	{
		private static List<Type> _knownTypes;
		//---------------------------------------------------------------------
		public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("GetKnownTypes");
			Console.ResetColor();

			return _knownTypes ?? (_knownTypes = GetKnownTypesCore().ToList());
		}
		//---------------------------------------------------------------------
		private static IEnumerable<Type> GetKnownTypesCore()
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("GetKnownTypesCore");
			Console.ResetColor();

			XElement registeredCommands = XElement.Load("./registeredCommands.xml");

			foreach (XElement command in registeredCommands.Elements("command"))
				yield return Type.GetType(command.Attribute("type").Value);
		}
	}
}