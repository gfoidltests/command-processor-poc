﻿using System;
using System.ServiceModel;

namespace Command_Processor_PoC.Server
{
	static class Program
	{
		static void Main(string[] args)
		{
			ServiceHost host = new ServiceHost(typeof(CommandProcessor), new Uri("net.pipe://localhost/commandProcessor"));
			try
			{
				host.Open();

				Console.WriteLine("Host running. Hit any key to shut down...");
				Console.ReadKey();

				host.Close();
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.Error.WriteLine(ex);
				Console.ResetColor();
				host.Abort();
			}

			Console.WriteLine("\nbye");
		}
	}
}