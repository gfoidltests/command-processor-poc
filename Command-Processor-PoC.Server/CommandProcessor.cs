﻿using System;
using System.Threading;
using Command_Processor_PoC.Contracts;

namespace Command_Processor_PoC.Server
{
	public class CommandProcessor : ICommandProcessor
	{
		public void Process(Command command)
		{
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine($"received message on T-ID {Thread.CurrentThread.ManagedThreadId} at {DateTime.Now}");
			Console.ResetColor();

			command.Do();
		}
	}
}